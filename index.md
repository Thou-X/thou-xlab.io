---
layout: default
title: "thouLee's Weblog"
page-title: thouLee's Weblog
home-title: Home
description: Main Page
---

{% include header.html %}
{% include boot.html %}
<main>
    {% include title.html %}
    <ul class="post-list">
      {% include posts_paginator.html %}
    </ul>
</main>
{% include pageNav.html %}
{% include footer.html %}
